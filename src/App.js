import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Navbar } from "./components/Navbar";
import { NotesList } from "./components/NotesList";
import { Tracker } from "./components/Tracker";
import { Note } from "./components/Note";
import { Provider } from 'react-redux'
import store from "./store";


function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="container">
          <Navbar />
          <br />
          <Route path="/" exact component={Tracker} />
          <Route path="/user/:id" component={Note} />
          <Route path="/list" component={NotesList} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
