import React from 'react';
import {Link} from 'react-router-dom';
import logo from '../assets/clock.jpg'

export const Navbar = () => {
  return (
    <nav className='navbar navbar-dark bg-dark navbar-expand-lg'>
      <div className='collpase navbar-collapse'>
        <ul className='navbar-nav mr-auto'>
          <li className='navbar-item' >
            <Link to='/' className='nav-link'>Tracker</Link>
          </li>
          <li className='navbar-item' >
            <Link to='/list' className='nav-link'>List of tracked items.</Link>
          </li>
          <img src={logo} className='logo-img' alt='logo' />
        </ul>

      </div>
    </nav>
  )
}