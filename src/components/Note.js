import React from 'react';
import { useSelector } from 'react-redux';

const Project = ({project}) => {
  return (
    <tr>
      <td>{project.projectName}</td>
      <td>{project.description}</td>
      <td>{project.time}</td>
    </tr>
  )
}

export const Note = ({history, match}) => {
  const users = useSelector(state => state.users)
  if (users.length === 0) history.push('/')
  const user = users.find(u => u.id === +match.params.id)

  const projectList = () => {
    return user?.projectsList.map(project => {
      return <Project key={new Date() + Math.random()} project={project} />
    })
  }

  return (
    <div>
      <h3>{user?.name}</h3>
      <table className='table'>
        <thead className='thead-light'>
          <tr>
            <th>Project name</th>
            <th>Description</th>
            <th>Log time</th>
          </tr>
        </thead>
        <tbody>{projectList()}</tbody>
      </table>
    </div>
  )
}