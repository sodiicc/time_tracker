import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const User = ({user}) => {
 const isProject = user.projectsList.length
  return (
    // On this page show only first the project but total time for user.
    <tr>
      <td><Link to={`/user/${user.id}`} className='nav-link'>{user.name}</Link></td>
      <td>{isProject ? user.projectsList[0].projectName : null}</td>
      <td>{isProject ? user.projectsList[0].description : null}</td>
      <td>{isProject ? user.projectsList.reduce((a, n) => a + +n.time, 0) : null}</td>
    </tr>
  )
}

export const NotesList = props => {
  const users = useSelector(state => state.users)
  if (users.length === 0) props.history.push('/')

  const noteList = () => {
    return users.map(user => {
      return <User key={user.id} user={user} />
    })
  }

  return (
    <div>
      <h3>Users List</h3>
      <table className='table'>
        <thead className='thead-light'>
          <tr>
            <th>User name</th>
            <th>Project name</th>
            <th>Description</th>
            <th>Total time</th>
          </tr>
        </thead>
        <tbody>{noteList()}</tbody>
      </table>
    </div>
  )
}