import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { setUsers } from '../actions/users'

const UsersOptions = ({users}) => {
  return (
    <>
      {
        users.map(user =>
        <option key={user.id} value={user.id}>
          {user.name}
        </option>)      
      }
    </>
  )
}

export const Tracker = () => {
  const users = useSelector(state => state.users)
  const [selectedUser, setSelectedUser] = useState(1)
  const [projectName, setProjectName] = useState('')
  const [time, setTime] = useState('')
  const [description, setDescription] = useState('')
  const [isAlert, setIsAlert] = useState(false)
  const dispatch = useDispatch();

  useEffect(() => {
    const lsUsers = JSON.parse(localStorage.getItem('users'))
    if (lsUsers) {
      dispatch(setUsers(lsUsers))
      setSelectedUser(lsUsers[0].id)
    }
    else {
      axios.get('https://jsonplaceholder.typicode.com/users')
      .then(res => {
        const modifyUsers = res.data.map(user => ({...user, projectsList: []}))
        setAllUsers(modifyUsers)
        setSelectedUser(modifyUsers[0].id)
      })
      .catch(err => console.log(err))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const clearFields = () => {
    setProjectName('')
    setTime('')
    setDescription('')
  }

  const setAllUsers = u => {
    dispatch(setUsers(u))
    localStorage.setItem('users', JSON.stringify(u))
  }

  const setAlert = () => {
    setIsAlert(true)
    setTimeout(() => setIsAlert(false), 2000)
  }

  const onSubmit = e => {
    e.preventDefault()
    if (projectName.trim !== '' && +time > 0) {
      const currentIndex = users.findIndex(user => user.id === +selectedUser)
      const newUsers = [...users]
      newUsers[currentIndex].projectsList.push({
        projectName,
        time,
        description
      })
      setAllUsers(newUsers)
      clearFields()
      setAlert()
    }
  }

  const onChangeUser = e => setSelectedUser(e.target.value)
  const onProjectNameChange = e => setProjectName(e.target.value)
  const onTimeChange = e => setTime(e.target.value)
  const onDescriptionChange = e => setDescription(e.target.value)

  return (
    <div>
      {
        isAlert ?
      <div className="alert alert-success" role="alert">
        Time logged successfully
      </div> :
      null
      }
      <h3>Log spending time</h3>
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label>Username: </label>
          <select
            required
            className="form-control"
            value={selectedUser}
            onChange={onChangeUser}
          >
            <UsersOptions users={users}/>
          </select>
        </div>
        <div className="form-group">
          <label>Project name: </label>
          <input
            type="text"
            required
            className="form-control"
            value={projectName}
            onChange={onProjectNameChange}
          />
        </div>
        <div className="form-group">
          <label>Time spent: </label>
          <input
            type="number"
            required
            className="form-control"
            value={time}
            onChange={onTimeChange}
          />
        </div>
        <div className="form-group">
          <label>Description: </label>
          <input
            type="text"
            className="form-control"
            value={description}
            onChange={onDescriptionChange}
          />
        </div>
        <div className="form-group">
          <input
            type="submit"
            value="Log time "
            className="btn btn-primary"
          />
        </div>
      </form>
    </div>
  )
}