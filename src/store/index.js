import { createStore, combineReducers } from 'redux'
import users from './users'

let Reducer = combineReducers({ users })
let store = createStore(Reducer)

export default store