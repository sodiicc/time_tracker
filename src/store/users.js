import { SET_USERS } from "../actions/types"

const userInitialState = []
const users = function (state = userInitialState, action) {
  switch (action.type) {
    case SET_USERS:
      return [ ...action.payload ]
    default:
      return state
  }
}

export default users